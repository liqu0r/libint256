#include "int256.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define run_test(func,msg,times) do{\
    if(times){\
        printf(msg);\
        unsigned i=(unsigned)(times);\
        for(;i>0;--i)\
            assert(!func());\
        printf("\t\t[PASS %u]\n",(unsigned)times);\
    }\
}while(0)

void rand_ull(unsigned long long *p)
{
    if(p){
        *p=0ULL;
        for(unsigned int i=0U;i<sizeof(unsigned long long);++i){
            *p<<=8;
            *p|=rand()&0xff;
        }
        *p^=0x1121ff0925ULL;
    }
}

int test_int256_clear(void)
{
    int256_t *p=new_int256();
    int ret=0;
    for(int i=0;i<32;++i){
        p->byte[i]=rand()&0xff;
    }
    int256_clear(p);
    for(int i=0;i<32;++i){
        if(p->byte[i]){
            ret=1;
            break;
        }
    }
    delete_int256(p);
    return ret;
}

int test_int256_compl(void)
{
    int256_t *p=new_int256();
    int256_t *q=new_int256();
    int ret=0;
    for(int i=0;i<32;++i){
        p->byte[i]=rand()&0xff;
    }
    memcpy(q->byte,p->byte,32);
    int256_compl(q);
    for(int i=0;i<32;++i){
        if((p->byte[i]^q->byte[i])!=(char)0xff){
            ret=1;
            break;
        }
    }
    delete_int256(q);
    delete_int256(p);
    return ret;
}

int test_int256_inc(void)
{
    unsigned long long n;
    rand_ull(&n);
    int256_t *p=new_int256();
    memcpy(p->byte,&n,sizeof(unsigned long long));
    int256_inc(p);
    int ret=(n+1==*((unsigned long long*)p->byte))?0:1;
    delete_int256(p);
    return ret;
}

int test_int256_dec(void)
{
    unsigned long long n;
    rand_ull(&n);
    int256_t *p=new_int256();
    memcpy(p->byte,&n,sizeof(unsigned long long));
    int256_dec(p);
    int ret=(n-1==*((unsigned long long*)p->byte))?0:1;
    delete_int256(p);
    return ret;
}

int test_int256_negative(void)
{
    int n=rand();
    int256_t *p=new_int256();
    memcpy(p->byte,&n,sizeof(int));
    int256_negative(p);
    int ret=(-n==*((int*)p->byte))?0:1;
    delete_int256(p);
    return ret;
}

int test_int256_add(void)
{
    unsigned long long a,b;
    rand_ull(&a);
    rand_ull(&b);
    int256_t *p=new_int256();
    int256_t *q=new_int256();
    memcpy(p->byte,&a,sizeof(unsigned long long));
    memcpy(q->byte,&b,sizeof(unsigned long long));
    int256_add(p,q);
    int ret=(a+b==*((unsigned long long*)p->byte))?0:1;
    delete_int256(q);
    delete_int256(p);
    return ret;
}

int test_int256_sub(void)
{
    unsigned long long a,b;
    rand_ull(&a);
    rand_ull(&b);
    int256_t *p=new_int256();
    int256_t *q=new_int256();
    memcpy(p->byte,&a,sizeof(unsigned long long));
    memcpy(q->byte,&b,sizeof(unsigned long long));
    int256_sub(p,q);
    int ret=(a-b==*((unsigned long long*)p->byte))?0:1;
    delete_int256(q);
    delete_int256(p);
    return ret;
}

int main(void)
{
    srand((unsigned)time(NULL));
    printf("Build on " __DATE__ ", " __TIME__ "\n");
    printf("Sizeof int256_t: %lu\n",sizeof(int256_t));
    assert(sizeof(int256_t)==32);
    run_test(test_int256_clear,"int256_clear",1);
    run_test(test_int256_compl,"int256_compl",1);
    run_test(test_int256_inc,"int256_inc",1);
    run_test(test_int256_dec,"int256_dec",1);
    run_test(test_int256_negative,"int256_negative",1);
    run_test(test_int256_add,"int256_add",1);
    run_test(test_int256_sub,"int256_sub",1);
    return 0;
}

