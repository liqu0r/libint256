#include "int256.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int256_t *new_int256(void)
{
    int256_t *ret=(int256_t*)calloc(1,sizeof(int256_t));
    if(ret==NULL){
        exit(EXIT_FAILURE);
    }
    return ret;
}

void delete_int256(int256_t *p)
{
    free(p);
}

void int256_clear(int256_t *p)
{
    memset(p->byte,0,32);
}

void int256_compl(int256_t *p)
{
    uint64_t *b=(uint64_t*)p->byte;
    b[0]=~b[0];
    b[1]=~b[1];
    b[2]=~b[2];
    b[3]=~b[3];
}

void int256_inc(int256_t *p)
{
    uint64_t *b=(uint64_t*)p->byte;
    for(int i=0;i<4;++i,++b){
        ++(*b);
        if(*b){
            return;
        }
    }
}

void int256_dec(int256_t *p)
{
    uint64_t *b=(uint64_t*)p->byte;
    for(int i=0;i<4;++i,++b){
        --(*b);
        if(~*b){
            return;
        }
    }
}

void int256_negative(int256_t *p)
{
    int256_compl(p);
    int256_inc(p);
}

void int256_add(int256_t *dst,const int256_t *src)
{
    unsigned int carry=0U;
    uint64_t *pd=(uint64_t*)dst->byte;
    const uint64_t *ps=(const uint64_t*)src->byte;
    uint64_t byte_sum=UINT64_C(0);
    for(int i=0;i<4;++i,++pd,++ps){
        byte_sum=*pd+*ps+carry;
        carry=(*ps>~*pd)?1U:0U;
        *pd=byte_sum;
    }
}

void int256_sub(int256_t *dst,const int256_t *src)
{
    unsigned long long borrow=0ULL;
    unsigned int *pd=(unsigned int*)dst->byte;
    const unsigned int *ps=(unsigned int*)src->byte;
    unsigned long long byte_diff=0ULL;
    int i=0;
    for(;i+sizeof(unsigned int)<=32;i+=sizeof(unsigned int),++pd,++ps){
        byte_diff=(unsigned long long)(*pd)-(unsigned long long)(*ps)-borrow;
        borrow=((*pd)<(*ps))?1ULL:0ULL;
        *pd=(unsigned int)byte_diff;
    }
    unsigned int _borrow=0U;
    unsigned int _byte_diff=0U;
    for(;i<32;++i){
        _byte_diff=(unsigned int)dst->byte[i]-(unsigned int)src->byte[i]-_borrow;
        _borrow=(dst->byte[i]<src->byte[i])?1U:0U;
        dst->byte[i]=(char)_byte_diff;
    }
}

void int256_print_bin(const int256_t *p)
{
    const char *byte=p->byte+31;
    for(int i=31;i>=0;--i){
        for(int j=7;j>=0;--j){
            putchar((*byte&(1<<j))?'1':'0');
        }
        --byte;
    }
}

