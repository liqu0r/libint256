CC=gcc
CFLAGS=-Werror -Wall -Wextra -std=c99 -pedantic -O2

a.out: *.c *.h
	$(CC) $(CFLAGS) *.c

test: a.out
	./a.out

