#ifndef _INT256_H_
#define _INT256_H_

typedef struct _int256_t{
    char byte[32];
}int256_t;

int256_t *new_int256(void);
void delete_int256(int256_t *p);
void int256_clear(int256_t *p);
void int256_compl(int256_t *p);
void int256_inc(int256_t *p);
void int256_dec(int256_t *p);
void int256_negative(int256_t *p);
void int256_add(int256_t *dst,const int256_t *src);
void int256_sub(int256_t *dst,const int256_t *src);
void int256_print_bin(const int256_t *p);

#endif

